package com.hcl.employee.contoller;

import java.util.Optional;

import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.employee.model.Employee;
import com.hcl.employee.service.EmployeeService;
import com.hcl.employee.util.ValidationErrorBuilder;
import com.hcl.employee.vo.EmployeeResponse;

@RestController
@RequestMapping(value="/employee")
public class EmployeeController {
	
	final static Logger logger = Logger.getLogger(EmployeeController.class);
	
	@Autowired
	EmployeeService employeeService;
	
	
	
	
	@RequestMapping(value="/addEmployee", method=RequestMethod.POST)
	public ResponseEntity<?> createEmployee(@Valid @RequestBody Employee employee, Errors errors){
		if (errors.hasErrors()) {
            return ResponseEntity.badRequest().body(ValidationErrorBuilder.fromBindingErrors(errors));
        }
		logger.info("Creating an Employee");
		return new ResponseEntity<EmployeeResponse>(employeeService.create(employee),HttpStatus.CREATED);
		
	}
	
	@RequestMapping(value="/getEmployee/{id}", method=RequestMethod.GET)
	public Optional<?> retreiveEmployee(@PathVariable("id") Long id) {
		
		logger.info("Retreiving an Employee");
		return employeeService.retreive(id);
		
	}
	
	@RequestMapping(value="/modifyEmployee", method=RequestMethod.PUT)
	public ResponseEntity<?> updateEmployee(@Valid @RequestBody Employee employee, Errors errors){
		if (errors.hasErrors()) {
            return ResponseEntity.badRequest().body(ValidationErrorBuilder.fromBindingErrors(errors));
        }
		logger.info("Updating an Employee");
		return new ResponseEntity<EmployeeResponse>(employeeService.update(employee),HttpStatus.OK);
		
	}
	
	@RequestMapping(value="/removeEmployee/{id}", method=RequestMethod.GET)
	public ResponseEntity<?>  deleteEmployee(@PathVariable("id") Long id) {
		
		logger.info("Deleting an Employee");
		return new ResponseEntity<EmployeeResponse>(employeeService.delete(id),HttpStatus.OK);
		
	}
	
	
}
