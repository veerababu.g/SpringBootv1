package com.hcl.employee.service;

import java.util.Optional;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hcl.employee.model.Employee;
import com.hcl.employee.repository.EmployeeRepository;
import com.hcl.employee.vo.EmployeeResponse;

@Service
public class EmployeeServiceImpl implements EmployeeService {
	
	@Autowired
	private EmployeeRepository employeeRepository;
	
	@Autowired
	private EmployeeResponse employeeResponse;
	
	final static Logger logger = Logger.getLogger(EmployeeServiceImpl.class);

	@Override
	public EmployeeResponse create(Employee employee) {
		
		employeeRepository.save(employee);
		employeeResponse.setStatus("Success");
		logger.info("Employee Created "+employeeResponse.getStatus()+"Succesfully");

		return employeeResponse;
	}

	@Override
	public Optional<Employee> retreive(Long id) {
		
		return employeeRepository.findById(id);
	}

	@Override
	public EmployeeResponse update(Employee employee) {

		employeeRepository.save(employee);
		employeeResponse.setStatus("Success");
		logger.info("Employee Modified "+employeeResponse.getStatus()+"Succesfully");

		return employeeResponse;
	}

	@Override
	public EmployeeResponse delete(Long id) {
		employeeRepository.deleteById(id);
		employeeResponse.setStatus("Success");
		logger.info("Employee Deleted "+employeeResponse.getStatus()+"Succesfully");

		return employeeResponse;
	}

}
