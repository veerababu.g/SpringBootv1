package com.hcl.employee.service;

import java.util.Optional;

import com.hcl.employee.model.Employee;
import com.hcl.employee.vo.EmployeeResponse;

public interface EmployeeService {
	
	EmployeeResponse create(Employee employee);
	
	Optional<Employee> retreive(Long id);
	
	EmployeeResponse update(Employee employee);
	
	EmployeeResponse delete(Long id);

}
