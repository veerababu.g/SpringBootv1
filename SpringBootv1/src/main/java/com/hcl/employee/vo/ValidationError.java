package com.hcl.employee.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;

public class ValidationError implements Serializable {
	
	 /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	    private List<String> errors = new ArrayList<>();

	    private final String errorMessage;

	    public ValidationError(String errorMessage) {
	        this.errorMessage = errorMessage;
	    }

	    public void addValidationError(String error) {
	        errors.add(error);
	    }

	    public List<String> getErrors() {
	        return errors;
	    }

	    public String getErrorMessage() {
	        return errorMessage;
	    }
}
